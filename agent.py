from twisted.web import server, resource, http
from twisted.internet import reactor
from common import *
from node.node import Node
import json, time
from task import execute_task

class Resource(resource.Resource):

    def __init__(self):
        resource.Resource.__init__(self)
        self.putChild("", self)
        self.putChild("node", Node())

    def render_GET(self, req):
        t = get_arg(req, "type")
        if not t:
            # heartbeat
            return json.dumps({"ret":0})
        elif t == "test":
            return json.dumps({"ret":0})

class HTTPChannel(http.HTTPChannel):

    def allHeadersReceived(self):
        http.HTTPChannel.allHeadersReceived(self)

class Site(server.Site):
    protocol = HTTPChannel

class main(object):

    def __init__(self):
        self.resource = Resource()
        self.site = Site(self.resource)
        agent_type = CONFIG['global']['agent_type']
        port = int(CONFIG['global']["%s_port" % agent_type])
        print "============="
        print "port:", port
        self.conn = reactor.listenTCP(port, self.site)
        self.run()

    def run(self):
        reactor.run()

if __name__ == "__main__":
    print "is_daemon:", is_daemon
    if is_daemon:
        be_daemon()
    execute_task()
    time.sleep(3)
    main()









