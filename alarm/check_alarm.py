#-*- coding=utf-8 -*-
from threading import Thread
from ganglia import get_specific_host_metric
from common import CONFIG
from alarm.mail import Email
from alarm.sms import send_sms
from common import host_collection, alarm_collection
import datetime, time

SMTP_HOST = "smtp.126.com"
FROM_ADDR = "ganglia_wonhigh@126.com"
PASSWORD = "ganglia654"

def if_exceed_threshold(metric_item, threshold, value):
    ret = None
    is_config_greater = int(CONFIG["threshold_greater"][metric_item])
    if is_config_greater:
        ret = value >= threshold
    else:
        ret = value <= threshold
    return ret

def send_alarm(hostname, metric, threshold, value):
    #
    subject = """WARNING: {metric_item} exceed threshold""".format(metric_item=metric)
    #
    msg = """
    hostname:      {hostname}
    metric item:   {metric_item}
    threshold:     {threshold}
    current value: {value}

    contact: nie.heng@wonhigh.cn
             18603071563""".format(hostname=hostname, metric_item=metric, threshold=threshold, value=value)
    # email
    if int(CONFIG['alarm']['email_on']):
        email = Email(SMTP_HOST)
        to_addrs = CONFIG['alarm']['to_addrs']
        email.send(FROM_ADDR, to_addrs, subject, msg, PASSWORD)
    # sms
    if int(CONFIG['alarm']['sms_on']):
        phones = CONFIG['alarm']['to_phones']
        send_sms(phones, msg)
    # update alarm history
    alarm_collection.insert({"hostname": hostname,
                             "metric": metric,
                             "value": value,
                             "threshold": threshold,
                             "datetime": datetime.datetime.now()})

def check_if_exceed_alarm_interval(hostname, metric):
    ret = False
    doc = alarm_collection.find_one({"hostname":hostname, "metric":metric})
    if doc:
        last_alarm = doc["datetime"]
        print "last_alarm;", last_alarm.strftime("%Y-%m-%d %H:%M")
        if (datetime.datetime.now() - last_alarm).seconds > 3600 * 6:
            ret = True
    else:
        ret = True
    return ret

class alarmThread(Thread):

    def __init__(self):
        Thread.__init__(self)

    def run(self):
        while True:
            time.sleep(int(CONFIG["alarm"]["interval"]))
            for host_detail in host_collection.find():
                print "============="
                print "host_detail:", host_detail
                hostname = host_detail["hostname"]
                for metric_item in host_detail["metric"]:
                    print "============="
                    print "metric_item:", metric_item
                    threshold = host_detail["metric"][metric_item]
                    print "threshold:", threshold
                    metric_value = get_specific_host_metric(hostname, metric_item)
                    print "%s: %s" % (metric_item, metric_value)
                    exceed_threshold = if_exceed_threshold(metric_item, threshold, metric_value)
                    print "exceed_threshold:", exceed_threshold
                    if exceed_threshold and check_if_exceed_alarm_interval(hostname, metric_item):
                        send_alarm(hostname, metric_item, threshold, metric_value)

