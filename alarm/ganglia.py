import sys
import traceback
import socket
import xml.parsers.expat

class GParser:
  def __init__(self, host, metric):
    self.inhost =0
    self.inmetric = 0
    self.value = None
    self.host = host
    self.metric = metric

  def parse(self, file):
    p = xml.parsers.expat.ParserCreate()
    # p.StartElementHandler = parser.start_element
    # p.EndElementHandler = parser.end_element
    p.StartElementHandler = self.start_element
    p.EndElementHandler = self.end_element
    p.ParseFile(file)
    if self.value == None:
      raise Exception('Host/value not found')
    return float(self.value)

  def start_element(self, name, attrs):
    if name == "HOST":
      if attrs["NAME"]==self.host:
        self.inhost=1
    elif self.inhost==1 and name == "METRIC" and attrs["NAME"]==self.metric:
      self.value=attrs["VAL"]

  def end_element(self, name):
    if name == "HOST" and self.inhost==1:
      self.inhost=0


def get_all_metric(ganglia_host, ganglia_port):
    all_metric = ""
    try:
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((ganglia_host, ganglia_port))
        all_metric = s.makefile("r")
        s.close()
    except Exception, e:
        traceback.print_exc()
    return all_metric

def get_metric_value(hostname, metric, all_metric):
    value = None
    try:
        parser = GParser(hostname, metric)
        value = parser.parse(all_metric)
    except Exception, e:
        traceback.print_exc()
    return value

def get_specific_host_metric(hostname, metric):
    ganglia_host = "172.17.209.2"
    ganglia_port = 8649
    # hostname = "dn209007"
    # metric = "disk_free"
    # warning = 30
    # critical = 10
    # opposite = 1

    all_metric = get_all_metric(ganglia_host, ganglia_port)
    value = get_metric_value(hostname, metric, all_metric)
    return value


if __name__ == "__main__":
    ganglia_host = "172.17.209.2"
    ganglia_port = 8649
    hostname = "dn209002"
    metric = "cpu_idle"
    warning = 30
    critical = 10
    opposite = 1

    all_metric = get_all_metric(ganglia_host, ganglia_port)
    print all_metric
    # cpu_idle = get_metric_value(hostname, metric, all_metric)
    # print cpu_idle
    # disk_free = get_metric_value(hostname, "disk_free", all_metric)
    # print disk_free
    root_disk_free = get_metric_value(hostname, "root_disk_free", all_metric)
    print root_disk_free






















