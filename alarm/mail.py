#-*- coding=utf-8 -*-
import smtplib
from email.mime.text import MIMEText
from email.header import Header

class Email(object):
    def __init__(self, host, port=25):
        self.host = host
        self.port = port

    def send(self, from_addr, to_addrs, subject, body, password=None):
        try:
            s = smtplib.SMTP(self.host, self.port)
            user = from_addr.split("@")[0].strip()
            s.login(user, password)
            msg = MIMEText(body, 'plain', 'utf-8')
            msg["Subject"] = subject
            msg["From"] = from_addr
            msg["To"] = to_addrs
            msg["Accept-Language"] = "zh-CN"
            msg["Accept-Charset"] = "ISO-8859-1,utf-8"
            s.sendmail(from_addr, to_addrs.split(','), msg.as_string())
            s.quit()
            return True
        except Exception, e:
            print str(e)
            return False

if __name__ == "__main__":
    email = Email("smtp.126.com")
    from_addr = "levinie001@126.com"
    to_addrs = "nie.heng@wonhigh.cn"
    password = ""
    for i in range(1):
        subject = "smtp test " + str(i + 1)
        msg = "This is test " + str(i + 1)
        ret = email.send(from_addr, to_addrs, subject, msg, password)
        if ret:
            print "===== Successful"
        else:
            print "===== Failed"
