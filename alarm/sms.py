import urllib2, urllib

def send_sms(phones, content):
    url = "http://172.17.210.215:3250/o2o-ms/send?sendType=1" \
          "&receivePhones={phones}&content={content}" \
          "&subject=&mainAddr=&ccAddr=&senderAddr=&emailMsgType=&emailPwd=".format(phones=phones, content=urllib.quote(content))
    print url
    urllib2.urlopen(url)

if __name__ == "__main__":
    phones = "18603071563"
    content = "OK"
    send_sms(phones, content)
