import os, sys, logging, ConfigParser, subprocess
import uuid, hashlib, platform, time, json
from pymongo import MongoClient

START_TIME = int(time.time())

def get_abs_path():
    return os.path.join(os.getcwd(), sys.argv[0])

ABS_PATH = get_abs_path()
ABS_DIR = os.path.dirname(ABS_PATH)
os.chdir(ABS_DIR)

is_daemon = False
CONFIG_PATH = os.path.join(ABS_DIR, "agent.ini")
CWD = os.getcwd()

# read config
# TODO: config default
def read_config(config_file):
    CONFIG = {}
    config = ConfigParser.SafeConfigParser()
    config.read(config_file)
    sections = config.sections()
    for section in sections:
        CONFIG[section] = {}
        options = config.options(section)
        for option in options:
            CONFIG[section][option] = config.get(section, option)
    return CONFIG

print CONFIG_PATH
CONFIG = read_config(CONFIG_PATH)

db_client = MongoClient(CONFIG["global"]["master_ip"])
db = db_client["monitor"]
host_collection = db["host"]
alarm_collection = db["alarm"]

logpath = CONFIG["global"]["logpath"]
if logpath:
    LOG_PATH = logpath
    tmp_dir = os.path.dirname(logpath)
    if not os.path.exists(tmp_dir):
        os.makedirs(tmp_dir)
else:
    LOG_PATH = os.path.join(ABS_DIR, "agent.log")

def init_log(logfile):
    logger = logging.getLogger()
    hdlr = logging.FileHandler(logfile)
    formatter = logging.Formatter('[%(asctime)s][%(levelname)s]: %(message)s')
    hdlr.setFormatter(formatter)
    logger.addHandler(hdlr)
    logger.setLevel(logging.NOTSET)
    return logger

logger = init_log(LOG_PATH)

def get_arg(req, name, default = None):
    if name in req.args:
        return req.args[name][0]
    return default

def be_daemon():
    try:
        pid = os.fork()
        if pid > 0:
            sys.exit(0)
    except OSError, e:
        print >>sys.stderr, 'catch exception:%s'%e
        sys.exit(1)
    os.umask(0)
    os.setsid()
    try:
        pid = os.fork()
        if pid > 0:
            sys.exit(0)
    except OSError, e:
        print >>sys.stderr, 'catch exception:%s'%e
        sys.exit(1)

def random_string():
    id = uuid.uuid1()
    m = hashlib.md5()
    m.update(str(id))
    m = m.hexdigest()
    return m

def get_lsb_release():
    return platform.linux_distribution()[0]




















