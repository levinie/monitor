import os

def get_root_disk_free(name):
    free = -1
    root_fields = []
    for l in os.popen("df -h"):
        fields = l.strip().split()
        if fields[-1] == "/":
            root_fields = fields
            break
    if not root_fields:
        return free
    free_with_unit = root_fields[-3]
    value, unit = float(free_with_unit[:-1]), free_with_unit[-1:]
    print value, unit
    if unit == "G":
        free = value
    elif unit == "T":
        free = value * 1024
    elif unit == "M":
        free = value / 1024
    return free

def metric_init(params):

    global descriptors

    d1 = {'name': 'root_disk_free',
        'call_back': get_root_disk_free,
        'time_max': 90,
        'value_type': 'float',
        'units': 'G',
        'slope': 'both',
        'format': '%f',
        'description': 'mounted disk free of specific directory'}

    descriptors = [d1]

    return descriptors

def metric_cleanup():
    pass

if __name__ == "__main__":
    # print get_root_disk_free('root_disk_free')
    #
    metric_init({})
    for d in descriptors:
        v = d['call_back'](d['name'])
        print "%s:" % d['name'], v

