from common import *
import httplib, json, time, os, socket

# TODO: default
TIMEOUT = int(CONFIG["monitor"]["timeout"])
INTERVAL = int(CONFIG["monitor"]["interval"])
RECURSION = 3
SCRIPT_PATH = os.path.join(ABS_DIR, "agent.py")

class Process:
    
    def __init__(self, flag, port):
        self.flag = flag
        self.port = port
        self.monitor()
    
    def get_pid(self):
        pid_list = []
        lines = pcmd("ps -ef | grep -v grep | grep %s" % self.flag).split("\n")
        for l in lines:
            if l.strip():
                pid = l.strip().split()[1]
                pid_list.append(pid)
        return pid_list
    
    def stop(self):
        pid_list = self.get_pid()
        for pid in pid_list:
            pcmd("kill -9 " + pid)
    
    def start(self):
        pcmd3("python " + SCRIPT_PATH)
    
    def heartbeat(self, recursion = RECURSION):
        ret = False
        try:
            conn = httplib.HTTPConnection("localhost", self.port, timeout = TIMEOUT)
            conn.request("GET", "/")
            res = conn.getresponse()
            if json.loads(res.read())["ret"] == 0:
                ret = True
        except socket.error, e:
            recursion -= 1
            if recursion > 0:
                ret = self.heartbeat(recursion = recursion)
        except socket.timeout, e:
            pass
        return ret
            
    def monitor(self):
        while True:
            ret = self.heartbeat()
            if not ret:
                self.stop()
                self.start()
            time.sleep(INTERVAL)
    
if __name__ == "__main__":
    be_daemon()
    port = CONFIG["global"]["port"]
    yhagent = Process("agent.py", port)



















