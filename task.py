from alarm.check_alarm import alarmThread

def execute_task():
    thread = alarmThread()
    thread.start()
    return True