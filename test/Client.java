import java.io.IOException;
import java.util.concurrent.*;
import java.io.InputStreamReader;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.File;
import java.net.URLEncoder;
import java.net.URL;
import java.net.HttpURLConnection;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.Iterator;

public class Client {
	
	public static Map<String, String> jsonToMap(JSONObject jsonObj) throws JSONException{
		Iterator it = jsonObj.keys();
		Map<String, String> ret = new HashMap<String, String>();
		for (int i = 0; it.hasNext(); i++){
			String k = it.next().toString();
			String v = jsonObj.getString(k);
			ret.put(k, v);
		}
		return ret;
	}

	public static String heartbeat(String ip, String port) throws IOException, JSONException {
	    String ret = "";
		try{
			String URL = "http://" + ip + ":" + port + "/heartbeat";
			//
			URL postUrl = new URL(URL);
			HttpURLConnection connection = (HttpURLConnection) postUrl
					.openConnection();
			connection.setDoOutput(true);
			connection.setRequestMethod("POST");
			connection.connect();
			BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
			String body;
			body = reader.readLine();
			reader.close();
			connection.disconnect();
			Map<String, String> retMap = jsonToMap(new JSONObject(body));
			ret = retMap.get("ret");
		}
		catch (IOException ex) {

		}
		return ret;


	}

	public static void main(String[] args) throws JSONException {
		try {
			String ip = "192.168.126.101";
			String port = "9651";
			String ret = heartbeat(ip, port);
			System.out.println("##### ret: " + ret);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}













