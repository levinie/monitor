import paramiko, os, logging, sys

def get_abs_path():
    return os.path.join(os.getcwd(), sys.argv[0])

ABS_PATH = get_abs_path()
ABS_DIR = os.path.dirname(ABS_PATH)

LOG_PATH = os.path.join(ABS_DIR, "deploy.log")

def init_log(logfile):
    logger = logging.getLogger()
    hdlr = logging.FileHandler(logfile)
    formatter = logging.Formatter('[%(asctime)s][%(levelname)s]: %(message)s')
    hdlr.setFormatter(formatter)
    logger.addHandler(hdlr)
    logger.setLevel(logging.NOTSET)
    return logger

logger = init_log(LOG_PATH)

class Client:
    
    def __init__(self, ip, port, user, password):
        self.ip = ip
        self.port = port
        self.user = user
        self.password = password
        
    def ssh(self, cmd):
        ret = ""
        try:
            conn=paramiko.SSHClient()
            conn.load_system_host_keys()
            conn.set_missing_host_key_policy(paramiko.AutoAddPolicy())
            conn.connect(self.ip, port = self.port, username = self.user, password = self.password, compress=True)
            stdin, stdout, stderr = conn.exec_command(cmd)
            ret = stdout.read()
            conn.close()
        except Exception, e:
            logger.error("##### ssh ERROR:")
            logger.error(e)
        return ret
    
    def scp(self, src, dst, method):
        try:
            conn=paramiko.Transport((self.ip, self.port))
            conn.connect(username = self.user, password = self.password)
            sftp=paramiko.SFTPClient.from_transport(conn)
            if method == "get":
                sftp.get(src, dst)
            if method == "put":
                sftp.put(src, dst)
            conn.close()
        except Exception, e:
            logger.error("##### scp ERROR:")
            logger.error(e)
            
def be_daemon():
    try:
        pid = os.fork()
        if pid > 0:
            sys.exit(0)
    except OSError, e:
        print >>sys.stderr, 'catch exception:%s'%e
        sys.exit(1)
    os.umask(0)
    os.setsid()
    try:
        pid = os.fork()
        if pid > 0:
            sys.exit(0)
    except OSError, e:
        print >>sys.stderr, 'catch exception:%s'%e
        sys.exit(1)
            
def get_ips():
    ips = []
    try:
        f = open("ip.ini")
        lines = f.readlines()
        for l in lines:
            l = l.strip()
            if l:
                print l
                ips.append(l)
    except Exception, e:
        logger.error("##### get_ips ERROR:")
        logger.error(e)
    return ips

def clear():
    #
    port = 22
    user = "root"
    password = "Mvtech@123!"
    #
    ips = get_ips()
    for ip in ips:
        print
        print ip
        c = Client(ip, port, user, password)
        ret = c.ssh("ls /opt")
        lines = ret.split("\n")
        for l in lines:
            tmp = l.strip().split()
            if tmp:
                name = tmp.pop()
                if len(name) == 32:
                    print name
                    c.ssh("rm -rf /opt/" + name)
            
if __name__ == "__main__":
#     be_daemon()
    clear()








