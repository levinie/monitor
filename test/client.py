# -*- coding=utf8 -*-
import httplib, sys, base64, json, urllib, hashlib
# from common import *
Debug = True

def get_res(conn):
    res=conn.getresponse()
    status=res.status
    reason=res.reason
    msg=res.msg
    if Debug:
        body=res.read()
    else:
        body = base64.b64decode(res.read())
    print status,reason
    print msg
    print body
    conn.close()
    return status,reason,msg,body

def do_http3(method, path, params=''):
    if path.startswith('http://'):  #去http://
        path = path.split('//')[1]
    path = path.rstrip('/') #去最后一个/
    if path.find('/') != -1:
        server, path = path.split('/', 1)
        path = '/'+path
    else:
        server, path = path, ''
    if server.find(':') != -1:
        host, port = server.split(':')
    else:
        host, port = server, 80
    conn = httplib.HTTPConnection(host,int(port))
    method = method.upper()
    if type(params) == dict:
        params = urllib.urlencode(params)
    if method == "POST":
        headers = {"Content-type": "application/x-www-form-urlencoded","Accept": "text/plain"}
        conn.request(method, path, params, headers) 
    else:
        if params:
            path = path + '?' + params
        conn.request(method, path) 
    resp = conn.getresponse()
    content = resp.read()
    return content

def b64(param):
    return base64.b64encode(param)

def test(method, url, body=None):
    conn = httplib.HTTPConnection("localhost", 8001)
    conn.request(method, url, body)
    get_res(conn)

if __name__ == "__main__":
    cmd = sys.argv[1]
    #cluster
    if cmd == "root":
        test("GET", "/")
    elif cmd == "status":
        test("POST", "/node?type=sca_status")






























