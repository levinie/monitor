import subprocess, sys

def pcmd(cmd):
    p = subprocess.Popen(cmd, stdout = subprocess.PIPE, stderr = subprocess.PIPE, shell = True)
    out = p.stdout.read()
    err = p.stderr.read()
    return out

def ps_and_kill(p):
    lines = pcmd("ps -ef | grep -v grep | grep %s" % p).split("\n")
    for l in lines:
        if l.strip():
            pid = l.split()[1]
            cmd = "kill -9 " + pid
            pcmd(cmd)

if __name__ == "__main__":
    args = sys.argv
    cmd = args[1]
    ps_and_kill(cmd)
